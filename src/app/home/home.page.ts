import { Component } from '@angular/core';
import { DecimalPipe } from '@angular/common';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})

export class HomePage {

  genders = [];
  gender: string;
  times = [];
  time: number;
  bottle = [];
  bottles: number;
  promilles: any;
  
  litres: number;
  grams: number;
  burning: number;
  gramsLeft: number;

private weight: number;

constructor() {
  this.weight = 0;
}


ngOnInit() {
  this.genders.push('Male');
  this.genders.push('Female');

  
  this.times.push('1');
  this.times.push('2');
  this.times.push('3');
  this.times.push('4');
  this.times.push('5');

  this.bottle.push('1');
  this.bottle.push('2');
  this.bottle.push('3');
  this.bottle.push('4');
  this.bottle.push('5');

  this.gender = 'Male';
  
}

calculate() { 
  this.litres = this.bottles * 0.33;
  this.grams = this.litres * 8 * 4.5;
  this.burning = this.weight / 10;
  this.gramsLeft = this.grams - (this.burning * this.time);

  if (this.gender === 'Male') {
    this.promilles = (this.gramsLeft / (this.weight * 0.7));
  } else {
    this.promilles = (this.gramsLeft / (this.weight * 0.6));
  }
}

}
